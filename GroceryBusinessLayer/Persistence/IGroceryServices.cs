﻿using System.Collections.Generic;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Persistence
{
    public interface IGroceryServices
    {
        public IEnumerable<Product> getallproducts();

        public Product GetproductDetails(int ProductId);
        
        //ApplicationUser PlaceOrder(ApplicationUser user, int id);
        public ProductOrder PlaceProductOrder(int productid, int userid);
        public IEnumerable<ProductOrder> GetOrderinfo(int UserId);

        public IEnumerable<Categories> GetListallcategories();
    }
}
