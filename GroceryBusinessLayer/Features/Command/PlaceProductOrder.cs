﻿using MediatR;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Command
{
    public class PlaceProductOrder : IRequest<ProductOrder>
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
    }
}
