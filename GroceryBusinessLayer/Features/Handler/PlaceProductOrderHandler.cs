﻿using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Command;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Persistence;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Handler
{
    public class PlaceProductOrderHandler : IRequestHandler<PlaceProductOrder, ProductOrder>
    {
        private readonly IGroceryServices _groceryServices;

        public PlaceProductOrderHandler(IGroceryServices groceryServices)
        {
            _groceryServices = groceryServices;
        }

        public async Task<ProductOrder> Handle(PlaceProductOrder request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_groceryServices.PlaceProductOrder(request.ProductId, request.UserId));
        }
    }
}
