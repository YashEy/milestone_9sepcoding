﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Query;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Persistence;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Repositories;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Handler
{
    public class GetproductDetailsHandler : IRequestHandler<GetproductDetails, Product>
    {
        private readonly IGroceryServices _groceryServices;

        public GetproductDetailsHandler(IGroceryServices groceryServices)
        {
            _groceryServices = groceryServices;
        }

        public Task<Product> Handle(GetproductDetails request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_groceryServices.GetproductDetails(request.ProductId));
        }
    }
}

