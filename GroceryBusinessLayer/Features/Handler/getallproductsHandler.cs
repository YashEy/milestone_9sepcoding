﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Query;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Persistence;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;
using MediatR;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Handler
{
    public class getallproductsHandler : IRequestHandler<getallproducts, IEnumerable<Product>>
    {
        private readonly IGroceryServices _groceryServices;

        public getallproductsHandler(IGroceryServices groceryServices)
        {
            _groceryServices = groceryServices;
        }

        public Task<IEnumerable<Product>> Handle(getallproducts request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_groceryServices.getallproducts());
        }
    }
}

   
