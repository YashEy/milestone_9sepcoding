﻿using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Query;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Persistence;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Repositories;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Handler
{
    public class GetOrderinfoHandler : IRequestHandler<GetOrderinfo, IEnumerable<ProductOrder>>
    {
        private readonly IGroceryServices _groceryServices;

        public GetOrderinfoHandler(IGroceryServices groceryServices)
        {
            _groceryServices = groceryServices;
        }

        public Task<IEnumerable<ProductOrder>> Handle(GetOrderinfo request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_groceryServices.GetOrderinfo(request.UserId));
        }
    }
}
