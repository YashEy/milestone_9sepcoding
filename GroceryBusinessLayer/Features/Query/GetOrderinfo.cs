﻿using MediatR;
using System.Collections.Generic;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Query
{
    public class GetOrderinfo : IRequest<IEnumerable<ProductOrder>>
    { 
        public int UserId { get; set; }
    }
}