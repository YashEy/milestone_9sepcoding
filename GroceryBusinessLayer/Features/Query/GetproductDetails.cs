﻿using MediatR;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Query
{
    public class GetproductDetails : IRequest<Product>
    {
        public int ProductId { get; set; }
    }
}
