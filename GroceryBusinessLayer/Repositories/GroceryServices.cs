﻿using System.Collections.Generic;
using System.Linq;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Persistence;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Repositories
{
    public class GroceryServices : IGroceryServices
    {
        private readonly GroceriesContext _groceriesContext;

        public GroceryServices(GroceriesContext groceriesContext)
        {
            _groceriesContext = groceriesContext;
        }

        public IEnumerable<Product> getallproducts()
        {
            return _groceriesContext.Product.ToList();
        }

        public IEnumerable<Categories> GetListallcategories()
        {
            return _groceriesContext.Categories.ToList();
        }

        public IEnumerable<ProductOrder> GetOrderinfo(int UserId)
        {
            return _groceriesContext.ProductOrder.Where(o => o.UserId == UserId).ToList();
        }

        public Product GetproductDetails(int ProductId)
        {
            var prod = _groceriesContext.Product.FirstOrDefault(o => o.ProductId == ProductId);
            return prod;
        }

        //public ApplicationUser PlaceOrder(ApplicationUser user, int id)
        //{
        //    throw new System.NotImplementedException();
        //}

        public ProductOrder PlaceProductOrder(int productid, int userid)
        {
            var OrderId = _groceriesContext.ProductOrder.Max(x => x.OrderId) + 1;
            var Product = _groceriesContext.Product.SingleOrDefault(x => x.ProductId == productid);
            var user = _groceriesContext.ApplicationUser.SingleOrDefault(x => x.UserId == userid);
            var productorder = new ProductOrder()
            {
                UserId = userid,
                ProductId = productid,
                OrderId = OrderId,
                Product = Product,
                User = user
            };
            _groceriesContext.ProductOrder.Add(productorder);
            _groceriesContext.SaveChanges();
            return productorder;
        }
    }
}