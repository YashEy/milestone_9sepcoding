﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Command;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryBusinessLayer.Features.Query;
using Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpGet]
        [Route("getallproducts")]

        public async Task<IEnumerable<Product>> Get()
        {
            return await _mediator.Send(new getallproducts());
        }

        // GET: api/<HomeController>
        [HttpGet("GetproductDetails")]
        public async Task<Product> Get(int id)
        {
            return await _mediator.Send(new GetproductDetails() { ProductId = id });
        }
        [HttpPost("PlaceProductOrder")]
        public async Task<ProductOrder> PlaceProductOrder(int productid, int userid)
        {
            return await _mediator.Send(new PlaceProductOrder() { ProductId = productid, UserId = userid });
        }

        [HttpGet("GetOrderinfo")]
        public async Task<IEnumerable<ProductOrder>> GetOrder(int id1)
        {
            return await _mediator.Send(new GetOrderinfo() { UserId = id1 });
        }
        //    // GET api/<HomeController>/5
        //    [HttpGet("{id}")]
        //    public string Get(int id)
        //    {
        //        return "value";
        //    }

        //    // POST api/<HomeController>
        //    [HttpPost]
        //    public void Post([FromBody] string value)
        //    {
        //    }

        //    // PUT api/<HomeController>/5
        //    [HttpPut("{id}")]
        //    public void Put(int id, [FromBody] string value)
        //    {
        //    }

        //    // DELETE api/<HomeController>/5
        //    [HttpDelete("{id}")]
        //    public void Delete(int id)
        //    {
        //    }
    }
}
