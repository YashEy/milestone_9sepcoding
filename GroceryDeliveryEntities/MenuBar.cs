﻿using System;
using System.Collections.Generic;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities
{
    public partial class MenuBar
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool? OpenInWindow { get; set; }
    }
}
