﻿using System;
using System.Collections.Generic;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities
{
    public partial class Categories
    {
        public Categories()
        {
            Product = new HashSet<Product>();
        }

        public int Catid { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
