﻿using System;
using System.Collections.Generic;

namespace Yash_CqrsMilestoneCodingTest_Sept09_3pm.GroceryDeliveryEntities
{
    public partial class ProductOrder
    {
        public int OrderId { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }

        public virtual Product Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
